

@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <div class="container">
        <style>
            .container{
                background-color: gray;
            }
        </style>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products') }} <style>
                            .card-header{
                                background-color: darkgray;
                            }
                        </style>
                        <form method="get" action="/products">
                            <input name="price" type="text"/>
                            <input value="Buscar" type="submit"/>
                        </form>
                    </div>
                    <div class="card-body">
                        @foreach ($products as $product)
                            <li>{{$product->nombre}}</li>
                            <li>{{$product->precio}}</li>
                            <li>{{$product->descripcion}}</li>
                            <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">Añadir al Carrito</a> </p>
                            <p class="btn-holder"><a href="{{ url('cart/') }}" class="btn btn-success btn-block text-center" role="button">Ver Carrito</a> </p>
                        <hr>
                            <style>
                                img{
                                    width: auto;
                                    height: 200px;
                                    margin: auto;
                                }
                                li{
                                    margin-left: 40%;
                                }
                                hr{
                                    background-color: black;
                                    height: 2px;
                                }
                            </style>
                        @endforeach
                        {{$products->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
@endsection

