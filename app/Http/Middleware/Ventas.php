<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Ventas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        // Comprobar que la cantidad < stock
        // consultar cantidad de cada producto
        return $next($request);
    }
}
