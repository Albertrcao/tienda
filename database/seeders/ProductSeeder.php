<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre' => 'product_name',
            'precio' => 99.90,
            'stock' => 100,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/laptop.jpg')),
        ]);
    }
}
