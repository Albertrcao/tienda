<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserModifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::table('users',function ($table){
           $table -> string('surname', 29);
        });
        schema::table('users',function ($table){
            $table -> string('dni', 9);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('users',function ($table){
            $table -> dropColumn('surname');
        });
        schema::table('users',function ($table){
            $table -> dropColumn('dni');
        });
    }
}
